# Citizen Client

The Citizen Client is an intuitive interface for users to authenticate, participate in the Agora platform as well as cast votes to the Echo VoDI Engine and Ballot-Box Smart Contracts.
